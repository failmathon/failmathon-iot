#!/usr/bin/env python3

from setuptools import setup, find_packages

from failmathon import __version__

setup(
    name='failmathon',
    version=__version__,
    install_requires=[
        'numpy',
        'aiohttp',
        'aioredis',
        'pillow'
    ],
    packages=find_packages(),
    scripts=['bin/failmathon', ]
)
