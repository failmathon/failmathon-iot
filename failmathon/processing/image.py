import asyncio
from typing import Optional, Tuple

import cv2
import numpy as np
from os import path

from failmathon.hardware.camera import Camera
from failmathon.networking.rest import RESTClient


class ImageProcessor:
    _threshold = 10

    def __init__(self, camera: Camera, api: RESTClient,
                 only_detect_movement: bool, stop_event: asyncio.Event):
        self._camera = camera
        self._api = api
        self._only_detect_movement = only_detect_movement
        self._stop_event = stop_event
        self._movement_detected_event = asyncio.Event()
        self._structing_elem = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                                         (3, 3))
        self._previous_frame = None  # type: Optional[np.ndarray]
        self._movement_frame = None  # type: Optional[np.ndarray]

    def _get_percentage_activity(self, frame: np.ndarray):
        difference = cv2.absdiff(self._previous_frame, frame)
        blurred = cv2.medianBlur(difference, 3)
        threshed = cv2.compare(blurred, 6, cv2.CMP_GT)

        eroded = cv2.erode(threshed, self._structing_elem)
        dilated = cv2.dilate(eroded, self._structing_elem)

        return dilated, cv2.countNonZero(dilated) * 100 / dilated.size

    @staticmethod
    def _get_object_bounding_box(frame: np.ndarray):
        _, contours, _ = cv2.findContours(frame, cv2.RETR_CCOMP,
                                          cv2.CHAIN_APPROX_SIMPLE)

        largest_contour = None
        largest_area = 0
        for contour in contours:
            area = cv2.contourArea(contour)
            if area > largest_area:
                largest_contour = contour
                largest_area = area

        return cv2.boundingRect(largest_contour)

    @staticmethod
    def _find_face(gray_image: np.ndarray):
        face_cascade = cv2.CascadeClassifier(path.join(path.expanduser('~'),
                                                       'classifier.xml'))

        faces = face_cascade.detectMultiScale(
            gray_image, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30)
        )

        if len(faces):
            return faces[0]

        return None

    def _process_frame(self) -> Optional[np.ndarray]:
        color_frame = self._camera.get_frame_unbuffered()
        gray_frame = cv2.cvtColor(color_frame, cv2.COLOR_BGR2GRAY)

        if self._previous_frame is not None:
            binary_frame, activity = self._get_percentage_activity(gray_frame)
            self._previous_frame = gray_frame

            if activity > self._threshold:
                face = self._find_face(gray_frame)
                if face is not None:
                    x, y, w, h = face

                    xp = x + w + 25
                    yp = y + h + 25
                    x -= 50
                    y -= 50

                    if (x > 0 and y > 0 and xp < gray_frame.shape[0] and
                                yp < gray_frame.shape[1]):
                        return color_frame[y:yp, x:xp]
        else:
            self._previous_frame = gray_frame

        return None

    def _detect_movement_in_frame(self) -> Tuple[bool, Optional[np.ndarray]]:
        color_frame = self._camera.get_frame_unbuffered()
        gray_frame = cv2.cvtColor(color_frame, cv2.COLOR_BGR2GRAY)

        if self._previous_frame is not None:
            binary_frame, activity = self._get_percentage_activity(gray_frame)
            self._previous_frame = gray_frame

            if activity > self._threshold:
                return True, color_frame
        else:
            self._previous_frame = gray_frame

        return False, None

    async def get_movement_image(self):
        await self._movement_detected_event.wait()
        self._movement_detected_event.clear()
        return self._movement_frame

    async def run(self):
        while not self._stop_event.is_set():
            if self._only_detect_movement:
                movement_present, frame = self._detect_movement_in_frame()
                if movement_present:
                    await self._api.send_frame(frame)
            else:
                image = self._process_frame()
                if image is not None:
                    await self._api.send_image(image)
                    pass

            await asyncio.sleep(1)
