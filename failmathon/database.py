from typing import Optional

import aioredis
from aioredis import RedisPool


class RedisDB:
    _connections = None  # type: Optional[RedisPool]

    @classmethod
    async def connect(cls, host='localhost', port=6379, db=0):
        cls._connections = await aioredis.create_pool((host, port), db=db)

    @classmethod
    def close(cls):
        cls._connections.close()

    @classmethod
    async def set_id(cls, device_id: str):
        with await cls._connections as redis:
            await redis.set('id', device_id)

    @classmethod
    async def get_id(cls) -> Optional[str]:
        with await cls._connections as redis:
            device_id = await redis.get('id')

            if device_id is None:
                return None

            return device_id.decode()
