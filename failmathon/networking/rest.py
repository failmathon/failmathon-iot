import base64
import io
import json
from typing import Union, Dict
from urllib.parse import urljoin
from uuid import getnode

import aiohttp
import cv2
import numpy as np
from PIL import Image

from failmathon.database import RedisDB
from failmathon.hardware.device import Capabilities


class RESTClient:
    def __init__(self, address: str):
        self._base_address = address
        self._session = aiohttp.ClientSession(
            headers={'content-type': 'application/json'}
        )

    def close(self):
        self._session.close()

    async def register(self):
        url = urljoin(self._base_address, 'Device/Register')
        data = json.dumps({
            'MacAddress': str(getnode()),
            'Capabilities': Capabilities.Accelerometer | Capabilities.Camera
        })

        async with self._session.post(url, data=data) as response:
            print(response.status)
            return await response.json()

    async def send_beacon_data(self, data: Dict[str, Union[bool, float]]):
        print('sending sensor data')

        url = urljoin(self._base_address, 'Sensor/Beacon')
        data = json.dumps(data)

        async with self._session.post(url, data=data) as response:
            print(response.status)
            return await response.json()

    async def send_image(self, image: np.ndarray):
        print('SENDING IMAGE')

        buffer = io.BytesIO()
        Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB)).save(buffer,
                                                                     'PNG')

        url = urljoin(self._base_address, 'Person/Recognize')
        data = json.dumps({
            'DeviceId': await RedisDB.get_id(),
            'EncodedImage': base64.b64encode(buffer.getvalue()).decode()
        })
        async with self._session.post(url, data=data) as response:
            status = response.status

            if status != 200:
                print(await response.json())
            else:
                print('IMAGE SENT')

    async def send_frame(self, image: np.ndarray):
        buffer = io.BytesIO()
        Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB)).save(buffer,
                                                                     'PNG')

        url = urljoin(self._base_address, 'Frame/Add')
        data = json.dumps({
            'DeviceId': await RedisDB.get_id(),
            'EncodedImage': base64.b64encode(buffer.getvalue()).decode()
        })
        async with self._session.post(url, data=data) as response:
            status = response.status

            if status != 200:
                print(await response.json())
            else:
                print('FRAME SENT')
