from enum import IntEnum


class Capabilities(IntEnum):
    Temperature = 0x01
    Humidity = 0x02
    Accelerometer = 0x04
    MagneticField = 0x08
    Camera = 0x10
