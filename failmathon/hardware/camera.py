import cv2


class Camera:
    def __init__(self, camera_number: int=0):
        self._camera = cv2.VideoCapture(camera_number)
        # self._camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        # self._camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

        for _ in range(30):
            self._camera.grab()

    def get_frame_unbuffered(self):
        # V4L2 5 frames buffer workaround
        for _ in range(0, 5):
            self._camera.grab()

        return self._camera.retrieve()[1]

    def get_frame(self):
        self._camera.grab()
        return self._camera.retrieve()[1]
