import asyncio
from asyncio.subprocess import PIPE
from typing import Optional, Tuple

import struct

from failmathon.database import RedisDB
from failmathon.networking.rest import RESTClient


class BeaconsListener:
    def __init__(self, api: RESTClient, available_event: asyncio.Event,
                 stop_event: asyncio.Event):
        self._api = api
        self._available_event = available_event
        self._stop_event = stop_event
        self._dumping_process = asyncio.create_subprocess_shell('hcidump --raw',
                                                                stdout=PIPE)
        self._scanning_process = asyncio.create_subprocess_shell(
            'hcitool lescan --duplicates --passive', stdout=PIPE
        )

    @staticmethod
    def _parse_value(s: str) -> int:
        n = len(s) / 2
        fmt = '%dh' % n
        pair_reversed = struct.pack(
            fmt, *reversed(struct.unpack(fmt, s.encode()))
        ).decode()

        return int(pair_reversed, 16)

    async def _read_data(self) -> Optional[Tuple[int, str]]:
        data = (await self._dumping_process.stdout.readuntil(b'>')).decode()
        # Remove '>' character, newlines and spaces; read until <
        data = data[:-1].replace('\n', '').replace(' ', '').split('<')[0][38:]

        device_id = data[2:4]
        if device_id:
            device_id = int(device_id, 16)
            if 0x93 <= device_id <= 0x95:
                return device_id, data

        return None

    def _check_motion(self, value: str) -> bool:
        return self._parse_value(value) == 0x8000

    def _parse_temperature(self, device_id: int, value: str) -> float:
        if device_id == 0x93:
            raw_value = self._parse_value(value)
            temperature = (702.88 * raw_value / 65536) - 46.85

            if temperature < -30:
                return -30
            if temperature > 70:
                return 70

            return temperature

    def _parse_humidity(self, device_id: int, value: str) -> float:
        if device_id == 0x93:
            raw_value = self._parse_value(value)
            humidity = (500 * raw_value / 65536) - 6

            if humidity < 1:
                return 1
            if humidity > 100:
                return 100

            return humidity

    async def _parse_data(self, device_id: int, data: str):
        if device_id == 0x93:
            # iNode #3
            return {
                'DeviceId': await RedisDB.get_id(),
                'InMotion': self._check_motion(data[12:16]),
                'Temperature': self._parse_temperature(device_id, data[16:20]),
                'Humidity': self._parse_humidity(device_id, data[20:24])
            }

        return None

    async def run(self):
        self._dumping_process = await self._dumping_process
        await asyncio.sleep(2)
        if self._dumping_process.returncode is not None:
            return

        self._available_event.set()
        self._scanning_process = await self._scanning_process

        while not self._stop_event.is_set():
            data = await self._read_data()
            if data is not None:
                parsed_data = await self._parse_data(*data)
                if parsed_data is not None:
                    await self._api.send_beacon_data(parsed_data)

        self._scanning_process.terminate()
        self._dumping_process.terminate()
